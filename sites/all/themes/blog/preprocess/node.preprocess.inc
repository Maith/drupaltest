<?php

function blog_preprocess_node(&$vars, $hook) {
  $date = date("d/m/Y", $vars['created']);
  $vars['submitted_teaser'] = $date;
  $name = $vars['name'];
  $vars['submitted'] = t('Written by !username at !date', array('!username' => $name, '!date' => $date));

  $vars['block'] = module_invoke('drupaltest', 'block_view', 'my_social');
  
}
