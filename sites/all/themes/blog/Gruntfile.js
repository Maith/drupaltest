module.exports = function (grunt) {

  // Load Grunt plugins
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-contrib-compass');

  var assetsFolder = "assets/";

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    autoprefixer: {
      options: {
        browsers: ['last 3 versions']
      },
      theme: {
        src: assetsFolder + 'css/blog.styles.css',
        dest: assetsFolder + 'css/blog.styles.css'
      },
    },
    compass: {
      dist: {
        options: {
          sassDir: 'sass',
          cssDir: 'css'
        }
      }
    },
    sass: {
      development: {
        options: {
          bundleExec: true,
          style: 'expanded',
          sourcemap: 'none',
          require: [
            'sass-globbing',
            'susy',
            'compass'
          ]
        },
        files: {
          'assets/css/blog.styles.css': assetsFolder + 'sass/screen.scss'
        }
      }
    },

    watch: {
      styles: {
        options: { livereload: true },
        files: ['**/*.scss'],
        tasks: ['sass', 'autoprefixer']
      }
    }
  });

  // Default task(s).
  grunt.registerTask('default', ['sass:development', 'autoprefixer', 'watch']);

};
