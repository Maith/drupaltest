<?php
/**
 * @file
 * general.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function general_filter_default_formats() {
  $formats = array();

  // Exported format: Plain text.
  $formats['plain_text'] = array(
    'format' => 'plain_text',
    'name' => 'Plain text',
    'cache' => 1,
    'status' => 1,
    'weight' => 10,
    'filters' => array(),
  );

  return $formats;
}
