<?php
/**
 * @file
 * general.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function general_user_default_roles() {
  $roles = array();

  // Exported role: Chief editor.
  $roles['Chief editor'] = array(
    'name' => 'Chief editor',
    'weight' => 2,
  );

  // Exported role: Editor.
  $roles['Editor'] = array(
    'name' => 'Editor',
    'weight' => 3,
  );

  return $roles;
}
