<?php
/**
 * @file
 * blogarticle.features.inc
 */

/**
 * Implements hook_node_info().
 */
function blogarticle_node_info() {
  $items = array(
    'blogarticle' => array(
      'name' => t('Blogarticle'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
